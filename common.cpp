#include "common.hpp"
#include <cassert>
#include <limits>

char toChar(TurnType turn_type)
{
    switch (turn_type)
    {
        case SKIP:
            return '.';
        case APPEND:
            return 'R';
        case PREPEND:
            return 'L';
        case INVALID:
            assert(0);
            return '\0';
    }
    assert(0);
    return '\0';
}

TurnType fromChar(char c)
{
    switch (c)
    {
        case '.':
            return SKIP;
        case 'R':
            return APPEND;
        case 'L':
            return PREPEND;
        default:
            return INVALID;
    }
}

std::ostream& operator<<(std::ostream& out, const interval_t& interval)
{
    out << toChar(interval.turn);
    out << ' ' << interval.start;
    out << ' ' << interval.end;
    return out;
}

std::istream& operator>>(std::istream& in, interval_t& interval)
{
    char c;
    in >> c >> interval.start >> interval.end;
    interval.turn = fromChar(c);
    return in;
}

std::ostream& operator<<(std::ostream& out, const interval_list_t& intervals)
{
    out << intervals.size();
    for (interval_list_const_iter_t it = intervals.cbegin(); it != intervals.cend(); ++it)
    {
        interval_t interval = *it;
        // last interval's end is extended to, effectively, +infinity
        if (it == intervals.cend() - 1)
        {
            interval.end = std::numeric_limits<decltype(it->end)>::max();
        }

        out << ' ' << interval;
    }
    return out;
}

std::istream& operator>>(std::istream& in, interval_list_t& intervals)
{
    size_t size;
    in >> size;

    intervals.resize(size);
    for (size_t i = 0; i < size; ++i)
    {
        in >> intervals[i];
    }
    return in;
}
