/*
 * Генератор таблицы, описанной в результатах первого обсуждения на
 * http://statmod.ru/wiki/study:fall2012:3adv_prog#добавь_букву,
 * для варианта игры с возможностью добавления как в начало, так и в конец.
 *
 * Вывод: файл precomputed_turns2.dat, состоящий из записей вида
 *          (инфикс + текущая буква) N (N интервалов)
 *
 *        Интервалы получаются посредством RLE-сжатия таблицы-советника.
 *        Интервал записывается в виде [op] [begin] [end], 
 *        где [op] - R (добавить в конец), L (добавить в начало), 
 *        или . (пропустить), а [begin] и [end] - такие числа, что 
 *        если [begin] <= [число оставшихся ходов] <= [end], 
 *        то нужно действовать согласно операции [op].
 *
 *        Например, запись
 *
 *          abilist 3 . 0 1 L 2 180 R 181 4294967295
 *
 *        означает, что при текущем инфиксе "abilis" букву 't' лучше добавлять
 *        в конец при большом числе оставшихся кодов (можно получить длинное слово 
 *        probabilist), а при меньшем лучше добавить её в начало (тогда можно
 *        собрать что-либо, связанное со stabilise).
 */
#include <array>
#include <memory>
#include <iostream>
#include <fstream>
#include <cassert>
#include <limits>
#include <string>
#include <vector>
#include <locale>
#include <cmath>

#include "trie.hpp"
#include "common.hpp"
#include "lengthexpectationarray.hpp"

class TwoSideGameLookupTableGenerator
{
    
    struct NodeData {

        NodeData() : is_visited(false)
        {
            parents.fill(nullptr);
        }

        LengthExpectationArray length_expectations;
        typedef std::array<TrieNode<NodeData>*, ALPHABET_SIZE> parents_t;
        parents_t parents;
        bool is_visited;
    };

    typedef Trie<NodeData> trie_t;
    typedef trie_t::node_type node_t;
    typedef node_t::children_type children_t;
    typedef children_t::const_iterator children_const_iter_t;
    typedef children_t::iterator children_iter_t;
    typedef NodeData::parents_t parents_t;

    trie_t trie;

public:

    const trie_t& getTrie() const
    {
        return trie;
    }

    void precomputeTurnsForDictionary(const std::string& filename)
    {
        trie.root = trie.createNewNode();
        std::ifstream in(filename);
        std::vector<node_t*> previous_word_path;

        while (!in.eof()) {
            std::string s;
            in >> s;

            previous_word_path.resize(s.length(), nullptr);

            node_t* node = trie.root;

            for (size_t i = 0; i < s.length(); ++i)
            {
                node_t* child = node->getChild(s[i]);
                if (child == nullptr)
                {
                    child = trie.createNewNode();
                    node->setChild(s[i], child);
                }

                node = child;
                previous_word_path[i] = node;
            }

            node->is_leaf = true;

            for (size_t i = 1; i < s.length(); ++i)
            {
                node_t* node = trie.root;

                for (size_t j = i; j < s.length(); ++j)
                {
                    node_t* child = node->getChild(s[j]);
                    if (child == nullptr)
                    {
                        child = trie.createNewNode();
                        node->setChild(s[j], child);
                    }

                    node = child;
                    node->data.parents[s[i-1] - 'a'] = previous_word_path[j - i + 1];
                    previous_word_path[j - i] = node;
                }
            }
        }

        precomputeExpectedLengths();
    }

    void serializePrecomputedTurnsToStream(std::ostream& out)
    {
        static struct {

            typedef std::pair<TurnType, LengthExpectationArray> TurnInfo;
            typedef std::vector<TurnInfo> OutcomeMap;

            TurnType getTurnType(const OutcomeMap& outcomes, size_t remaining)
            {
                size_t index_of_best_expectation = 0;

                for (size_t i = 1; i < outcomes.size(); ++i)
                {
                    float best_expectation = outcomes[index_of_best_expectation].second[remaining];
                    if (outcomes[i].second[remaining] > best_expectation)
                    {
                        index_of_best_expectation = i;
                    }
                }

                return outcomes[index_of_best_expectation].first;
            }

            interval_list_t getIntervals(const OutcomeMap& outcomes, len_t current_depth)
            {

                interval_list_t intervals;
                interval_t current_interval = { SKIP, 0, 0 };

                current_interval.turn = getTurnType(outcomes, 0);

                size_t n = outcomes[0].second.size() - (current_depth + 1);

                for (size_t i = 1; i < n; ++i)
                {
                    TurnType turn = getTurnType(outcomes, i);
                    if (turn != current_interval.turn)
                    {
                        intervals.push_back(current_interval);
                        current_interval.turn = turn;
                        current_interval.start = i;
                    }

                    current_interval.end = i;
                }

                intervals.push_back(current_interval);
                return intervals;
            }

            void visit(const node_t* node, std::ostream& out, const std::string& prefix)
            {
                if (node == nullptr)
                {
                    return;
                }

                for (char c = 'a'; c <= 'z'; ++c)
                {
                    node_t* child = node->getChild(c);
                    node_t* parent = node->data.parents[c - 'a'];

                    if (child == nullptr && parent == nullptr)
                    {
                        continue;
                    }

                    OutcomeMap outcomes;
                    outcomes.push_back(std::make_pair(SKIP, node->data.length_expectations));

                    if (child != nullptr)
                    {
                        outcomes.push_back(std::make_pair(APPEND, child->data.length_expectations));
                    }

                    if (parent != nullptr)
                    {
                        outcomes.push_back(std::make_pair(PREPEND, parent->data.length_expectations));
                    }

                    std::string tmp = prefix + c;

                    interval_list_t intervals = getIntervals(std::move(outcomes), prefix.size());

                    out << tmp << ' ' << intervals << '\n';

                    if (child != nullptr)
                    {
                        visit(child, out, tmp);
                    }
                }
            }
        } visitor;

        visitor.visit(trie.root, out, "");
    }

private:
    void precomputeExpectedLengths()
    {
        static struct {
            /* computes trienode length expectations */
            inline void operator()(node_t* trienode, size_t current_depth)
            {
                /* precompute expected lengths for all children */
                children_t& children = trienode->children;
                for (children_iter_t it = children.begin(); it != children.end(); ++it)
                {
                    node_t* child = *it;
                    if (child != nullptr && !child->data.is_visited)
                    {
                        operator()(child, current_depth + 1);
                    }
                }

                /* do the same for parents */
                parents_t parents = trienode->data.parents;
                for (parents_t::iterator it = parents.begin(); it != parents.end(); ++it)
                {
                    node_t* parent = *it;
                    if (parent != nullptr && !parent->data.is_visited)
                    {
                        operator()(parent, current_depth + 1);
                    }
                }

                /* no more recursive calls, mark current node as visited */
                trienode->data.is_visited = true;

                /* set value for remaining=0 */
                trienode->data.length_expectations[0] = trienode->is_leaf ? static_cast<float>(current_depth) : 0.0f;

                /* precompute expected lengths for the current node */
                for (size_t remaining = 1; remaining <= LengthExpectationArray::turns_to_precompute - current_depth; ++remaining)
                {
                    float sum = 0.0;
                    float skip_length_expectation = trienode->data.length_expectations[remaining - 1];

                    for (size_t i = 0; i < ALPHABET_SIZE; ++i)
                    {
                        node_t* child = trienode->children[i];
                        node_t* parent = trienode->data.parents[i];

                        float result = skip_length_expectation;

                        if (child != nullptr)
                        {
                            float append_length_expectation = child->data.length_expectations[remaining - 1];
                            if (append_length_expectation > result)
                            {
                                result = append_length_expectation;
                            }
                        }

                        if (parent != nullptr)
                        {
                            float prepend_length_expectation = parent->data.length_expectations[remaining - 1];
                            if (prepend_length_expectation > result)
                            {
                                result = prepend_length_expectation;
                            }
                        }
                        
                        sum += result;
                    }

                    trienode->data.length_expectations[remaining] = sum / ALPHABET_SIZE;
                }
            }
        } helper;

        if (trie.root != nullptr)
        {
            helper(trie.root, 0);
        }
    }
};

int main(int argc, const char* argv[])
{
    TwoSideGameLookupTableGenerator gen;

    if (argc == 2)
    {
        LengthExpectationArray::turns_to_precompute = std::atoi(argv[1]);
    }

    gen.precomputeTurnsForDictionary("dict2.txt");
    std::cout << "[precompute2] Precomputed turns = " << LengthExpectationArray::turns_to_precompute
              << ", expected average length = "
              << gen.getTrie().root->data.length_expectations[LengthExpectationArray::turns_to_precompute] 
              << std::endl;

    std::ofstream out("precomputed_turns2.dat");
    gen.serializePrecomputedTurnsToStream(out);

    return 0;
}
