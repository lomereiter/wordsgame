/*
 * Реализация стратегии, описанной в результатах второго обсуждения на
 * http://statmod.ru/wiki/study:fall2012:3adv_prog#добавь_букву,
 * для варианта игры с добавлением только в конец.
 *
 * Для каждого K, такого что { w из словаря | длина w равна K } непусто,
 * выводит все префиксы, в которые нужно переходить для минимизации
 * математического ожидания времени, за которое будет собрано слово длины K
 * при бесконечном количестве оставшихся букв.
 */
#include <array>
#include <memory>
#include <iostream>
#include <fstream>
#include <cassert>
#include <limits>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <locale>
#include <cmath>

#include "trie.hpp"
#include "common.hpp"

typedef Trie<float> trie_t;
typedef trie_t::node_type node_t;
typedef node_t::children_type children_t;
typedef children_t::iterator children_iter_t;
typedef children_t::const_iterator children_const_iter_t;

void precomputeExpectedTimes(trie_t& trie)
{
    static struct {

        /*
         *
         * T(node) = 1 + average(min(T(node), T(child)))
         *
         * T(nullptr) = +infinity
         * T(leaf) = 0
         *
         * Algorithm:
         *  1) sort children by expected time
         *          [T1, T2, T3, ...,                           +inf, +inf, ...]
         *          { n children with finite expected times } { ALPHABET_SIZE - n} 
         *
         *  2) suppose T_k <= T < T_{k+1}
         *
         *     Then equation for T is
         *     T = (T1 + ... + T_k + (ALPHABET_SIZE - k) * T) / ALPHABET_SIZE + 1
         *     k * T = T1 + ... + T_k + ALPHABET_SIZE
         *     f(k) = (T1 + ... + T_k + ALPHABET_SIZE) / k
         *
         *     T(node) = min(f(k))
         */

        inline void operator()(trie_t::node_type* trienode, unsigned int current_depth)
        {
            size_t n = 0; 
            std::array<float, ALPHABET_SIZE> expected_times;

            /* precompute expected lengths for all children */
            children_t& children = trienode->children;
            for (children_iter_t it = children.begin(); it != children.end(); ++it)
            {
                node_t* child = *it;
                if (child != nullptr)
                {
                    operator()(child, current_depth + 1);
                    expected_times[n++] = child->data;
                }
            }

            std::sort(expected_times.begin(), expected_times.begin() + n);

            if (trienode->is_leaf)
            {
                trienode->data = 0.0;
                return;
            }
            
            /* precompute expected time for the current node */
            float best_time = std::numeric_limits<float>::infinity();

            float accumulator = 0.0;
            for (size_t k = 0; k < n; ++k)
            {
                accumulator += expected_times[k];
                float time = (accumulator + ALPHABET_SIZE) / (k + 1);
                if (time < best_time)
                {
                    best_time = time;
                }
            }

            trienode->data = best_time;
        }
    } helper;

    if (trie.root != nullptr)
    {
        helper(trie.root, 0);
    }
}

void savePrecomputedTurns(const trie_t& trie, std::ostream& out)
{
    static struct {
        void visit(const trie_t::node_type* node, 
                   std::ostream& out,
                   const std::string& prefix)
        {
            if (node == nullptr)
            {
                return;
            }

            for (char c = 'a'; c <= 'z'; ++c)
            {
                node_t* child = node->getChild(c);

                if (child != nullptr && child->data < node->data)
                {
                    std::string tmp = prefix + c;
                    out << tmp << std::endl;
                    visit(child, out, tmp);
                }
            }
        }
    } visitor;

    visitor.visit(trie.root, out, "");
}

namespace detail
{
    typedef std::string word_t;
    typedef std::vector<word_t> word_list_t;
    typedef word_list_t::const_iterator word_list_const_iter_t;
    typedef std::map<size_t, word_list_t> dict_bundle_t;
    typedef dict_bundle_t::const_iterator dict_bundle_const_iter_t;
}

int main()
{
    std::ofstream out("precomputedTimes1.dat");

    std::ifstream in("dict2.txt");
    detail::dict_bundle_t words;

    while (!in.eof())
    {
        word_t word;
        in >> word;
        words[word.length()].push_back(word);
    }

    for (detail::dict_bundle_const_iter_t it = words.cbegin(); it != words.cend(); ++it)
    {
        size_t K = it->first;
        detail::word_list_t words_of_length_K = it->second;

        if (K == 0) continue;

        trie_t trie;

        for (detail::word_list_const_iter_t it = words_of_length_K.cbegin(); it != words_of_length_K.cend(); ++it)
        {
            word_t& s = *it;
        
            trie_t subtrie = trie.getSubtrie(s, true);
            subtrie.root->is_leaf = true;
        }

        precomputeExpectedTimes(trie);
        out << "#length " << K << ' ' << trie.root->data << std::endl;
        savePrecomputedTurns(trie, out);
    }

    return 0;
}
