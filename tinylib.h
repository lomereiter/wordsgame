#ifndef TINYLIB_H
#define TINYLIB_H

#include <string>
#include <set>
#include <random>

using namespace std;

const int ALP_SZ = 26;
const int ALP_F = 0x61;
const int MAXGAME = 1000;
const int MAXSET = 1000000;
const double CGAMMA = 1.96; //gamma = 0.95
class Game {

private:
  string cliname;
  int named;
  int ingame;
  int setnum;
  int gametype;
  int gamecount;
  int gamelength;
  int curgame;
  int curpos;
  string curword;
  int waitforans;
  char cursymb;
  ofstream blog;
  int sumlength;
  int sumcount;

  set<string> words;
  char quests[MAXGAME];
  char answs[MAXGAME];
  int lengths[MAXSET];

  //mersenne_twister gen;
  mt19937 gen;
  uniform_int_distribution<int> dist;

  
  static string to_string(int a);

  string get_game_type();

  void start_log();

  void write_move_map();

  void insert_succ(string word, int num);

  void insert_fail(string word, int num);

  void finish_log();


  void insert_word(string word);

  int have_word(string word);

public:

  Game(string name = "anonymous");

  int start_test_set(int type, int game_count, int length, int seed);

  int start_set(int type, int game_count, int length);

  char get_char();

  int put_ans(int ans);

  int end_game();

};

#endif
