#ifndef TRIE_HPP
#define TRIE_HPP

#include <array>
#include <memory>
#include <locale>
#include <string>
#include <utility>

#include "common.hpp"

template <typename T>
struct TrieNode
{
    TrieNode() : is_leaf(false)
    {
        children.fill(nullptr);
    }

    TrieNode* getChild(char c) const
    {
        if (!islower(c))
            return nullptr;

        return children[c - 'a'];
    }

    void setChild(char c, TrieNode* value) 
    {
        if (islower(c)) 
        {
            children[c - 'a'] = value; 
        }
    }

    bool is_leaf;
    T data;

    typedef std::array<TrieNode*, ALPHABET_SIZE> children_type;
    children_type children;
};

template <typename T>
struct Trie
{
    typedef T data_type;
    typedef TrieNode<T> node_type;
    typedef typename node_type::children_type node_children_type;
    typedef typename node_children_type::const_iterator node_children_const_iter_type;

    node_type* root;

    Trie() : root(nullptr), _must_delete_nodes(true), _size(0)
    {
    }

    ~Trie()
    {
        if (_must_delete_nodes)
        {
            _recursive_delete_trie_nodes(root);
        }
    }

    private:

    Trie(node_type* ptr) : root(ptr), _must_delete_nodes(false)
    {
    }
    
    Trie(node_type* ptr, bool own) : 
        root(ptr), 
        _must_delete_nodes(own)
    {
    }

    bool _must_delete_nodes;

    void _recursive_delete_trie_nodes(node_type* node)
    {
        if (node == nullptr)
        {
            return;
        }
        
        node_children_type& children = node->children;
        for (node_children_const_iter_type it = children.begin(); it != children.end(); ++it)
        {
            node_type* child = *it;
        
            if (child != nullptr)
            {
                _recursive_delete_trie_nodes(child);
            }
        }

        delete node;
        --_size;
    }
    
    public:
    Trie getSubtrie(const std::string& s, bool create_if_doesnt_exist=false)
    {
        if (create_if_doesnt_exist && root == nullptr)
        {
            root = createNewNode();
        }

        node_type* ptr = root;

        for (std::string::const_iterator it = s.cbegin(); it != s.cend(); ++it)
        {
            char c = *it;
        
            if (ptr == nullptr)
            {
                break;
            }

            node_type* child_ptr = ptr->getChild(c);
            if (child_ptr == nullptr)
            {
                if (create_if_doesnt_exist)
                {
                    child_ptr = createNewNode();
                    ptr->setChild(c, child_ptr);
                }
                else
                {
                    return Trie(nullptr, false);
                }
            }

            ptr = child_ptr;
        }
        return Trie(ptr, false);
    }

    node_type* createNewNode()
    {
        node_type* ptr = new node_type;
        ++_size;
        return ptr;
    }

    size_t size()
    {
        return _size;
    }

private:

    size_t _size;
};
#endif
