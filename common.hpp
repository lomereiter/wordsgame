#ifndef COMMON_HPP
#define COMMON_HPP

#include <cstdint>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>

static const unsigned int ALPHABET_SIZE = 26;

static const unsigned int PRECOMPUTE_TURNS = 200;

typedef uint16_t len_t;
enum TurnType : uint8_t
{
    SKIP,
    APPEND,
    PREPEND,
    INVALID
};

char toChar(TurnType turn_type);

TurnType fromChar(char c);

enum GameType
{
    ONESIDE,
    TWOSIDE
};

// when we are staying in the parent node, 
// and the number of characters left is in [start .. end] interval, 
// we should take the action described by 'turn'
struct interval_t
{
    TurnType turn;
    unsigned int start;
    unsigned int end;
};

typedef std::vector<interval_t> interval_list_t;
typedef interval_list_t::const_iterator interval_list_const_iter_t;

std::ostream& operator<<(std::ostream& out, const interval_t& interval);

std::istream& operator>>(std::istream& in, interval_t& interval);

std::ostream& operator<<(std::ostream& out, const interval_list_t& intervals);

std::istream& operator>>(std::istream& in, interval_list_t& intervals);
#endif
