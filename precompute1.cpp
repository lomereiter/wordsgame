/*
 * Генератор таблицы, описанной в результатах первого обсуждения на
 * http://statmod.ru/wiki/study:fall2012:3adv_prog#добавь_букву,
 * для варианта игры с возможностью добавления только в конец.
 *
 * Вывод: файл precomputed_turns1.dat, состоящий из записей вида
 *          (префикс + текущая буква) N (N интервалов)
 *
 *        Интервалы получаются посредством RLE-сжатия таблицы-советника.
 *        Интервал записывается в виде [op] [begin] [end], 
 *        где [op] - либо R (добавить), либо . (пропустить), а [begin] и [end] - 
 *        такие числа, что если [begin] <= [число оставшихся ходов] <= [end], 
 *        то нужно действовать согласно операции [op].
 *
 *        Например, запись
 *
 *          abandone 3 . 0 53 R 54 125 . 126 4294967295
 *
 *        означает, что при текущем префиксе "abandon" букву 'e' стоит брать, если
 *        количество оставшихся ходов лежит в диапазоне от 54 до 125, иначе лучше
 *        её пропустить. 
 *        (Если остаётся меньше ходов - шансы собрать слово abandoned меньше, чем
 *        получить abandon или abandons; если больше - выгодней попытаться собрать 
 *        слово abandonment.)
 */
#include <array>
#include <memory>
#include <iostream>
#include <fstream>
#include <cassert>
#include <limits>
#include <string>
#include <vector>
#include <locale>
#include <cmath>
#include <cstdlib>

#include "trie.hpp"
#include "common.hpp"
#include "lengthexpectationarray.hpp"

class OneSideGameLookupTableGenerator
{
    typedef Trie<LengthExpectationArray> trie_t;
    typedef trie_t::node_type node_t;
    typedef node_t::children_type children_t;
    typedef children_t::const_iterator children_const_iter_t;
    typedef children_t::iterator children_iter_t;

    trie_t trie;

public:

    const trie_t& getTrie() const 
    {
        return trie;
    }

    void precomputeTurnsForDictionary(const std::string& filename)
    {
        std::ifstream in(filename);

        if (!in)
        {
            std::cerr << "File '" << filename << "' not found" << std::endl;
            std::exit(1);
        }

        while (!in.eof()) {
            std::string s;
            in >> s;
            trie_t subtrie = trie.getSubtrie(s, true);
            subtrie.root->is_leaf = true;
        }

        precomputeExpectedLengths();
    }

    void serializePrecomputedTurnsToStream(std::ostream& out)
    {
        static struct {

            TurnType getTurnType(float parent_expectation, float child_expectation)
            {
                return parent_expectation > child_expectation ? SKIP : APPEND;
            }

            interval_list_t getIntervals(const trie_t::data_type& parent_length_expectations,
                                         const trie_t::data_type& child_length_expectations,
                                         len_t child_prefix_length)
            {
                interval_list_t intervals;
                interval_t current_interval = { SKIP, 0, 0 };

                current_interval.turn = getTurnType(parent_length_expectations[0], 
                                                    child_length_expectations[0]);

                // if child prefix length is L, last L elements of its data are uninitialized
                size_t n = child_length_expectations.size() - child_prefix_length;
                for (size_t i = 1; i < n; ++i)
                {
                    TurnType turn = getTurnType(parent_length_expectations[i], child_length_expectations[i]);
                    if (turn != current_interval.turn)
                    {
                        intervals.push_back(current_interval);
                        current_interval.turn = turn;
                        current_interval.start = i;
                    }

                    current_interval.end = i;
                }

                intervals.push_back(current_interval);
                return intervals;
            }

            void visit(const trie_t::node_type* node, 
                       std::ostream& out,
                       const std::string& prefix)
            {
                if (node == nullptr)
                {
                    return;
                }

                for (char c = 'a'; c <= 'z'; ++c)
                {
                    node_t* child = node->getChild(c);

                    if (child != nullptr)
                    {
                        std::string tmp = prefix + c;

                        interval_list_t intervals = getIntervals(node->data, child->data, tmp.size());

                        out << tmp << ' ' << intervals << '\n';
                        
                        visit(child, out, tmp);
                    }
                }
            }
        } visitor;

        visitor.visit(trie.root, out, "");
    }

private:

    void visit(node_t* trienode, unsigned int current_depth)
    {
        /* precompute expected lengths for all children */
        children_t& children = trienode->children;
        for (children_iter_t it = children.begin(); it != children.end(); ++it)
        {
            node_t* child = *it;
            if (child != nullptr)
            {
                visit(child, current_depth + 1);
            }
        }

        /* set value for remaining=0 */
        trienode->data[0] = trienode->is_leaf ? static_cast<float>(current_depth) : 0.0f;

        /* precompute expected lengths for the current node */
        for (unsigned int remaining = 1; remaining <= LengthExpectationArray::turns_to_precompute - current_depth; ++remaining)
        {
            float sum = 0.0;
            float skip_length_expectation = trienode->data[remaining - 1];

            children_t& children = trienode->children;
            for (children_iter_t it = children.begin(); it != children.end(); ++it)
            {
                node_t* child = *it;
            
                if (child == nullptr)
                {
                    sum += skip_length_expectation;
                }
                else 
                {
                    float append_length_expectation = child->data[remaining - 1];
                    sum += std::max(append_length_expectation, skip_length_expectation);
                }
            }

            trienode->data[remaining] = sum / ALPHABET_SIZE;
        }
    }

    void precomputeExpectedLengths()
    {
        if (trie.root != nullptr)
        {
            visit(trie.root, 0);
        }
    }
};

int main(int argc, const char* argv[])
{
    OneSideGameLookupTableGenerator gen;

    if (argc == 2)
    {
        LengthExpectationArray::turns_to_precompute = std::atoi(argv[1]);
    }

    gen.precomputeTurnsForDictionary("dict2.txt");

    std::cout << "[precompute1] Precomputed turns = " << LengthExpectationArray::turns_to_precompute
              << ", expected average length = " << gen.getTrie().root->data[LengthExpectationArray::turns_to_precompute] 
              << std::endl;

    std::ofstream out("precomputed_turns1.dat");
    gen.serializePrecomputedTurnsToStream(out);

    return 0;
}
