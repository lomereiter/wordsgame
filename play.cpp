/*
 * Реализация игры с использованием оптимального решения.
 * 
 * При запуске сперва создаётся бор, позволяющий за O(1)
 * определять, какую операцию предпочтительней выполнить
 * при поступлении следующей буквы. Предрассчитанная таблица
 * считывается из файла precomputed_turnsK.dat 
 * (где K=1 для варианта игры с добавлением только в конец,
 * K=2 для варианта игры с добавлением как в начало, так и в конец).
 *
 * Далее производится серия игр (10000) с заданным числом ходов в
 * каждой игре. Лог сохраняется в файл, имя которого выводится
 * на экран по завершении моделирования.
 */
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cassert>
#include <cstdlib>

#include "trie.hpp"
#include "common.hpp"

#include "tinylib.h"

class LookupTable
{
    typedef Trie<std::vector<interval_t>> trie_t;

    trie_t trie;

public:
    void loadFromFile(const std::string& filename)
    {
        std::ifstream in(filename);

        if (!in)
        {
            std::cerr << "[error] File not found: " << filename << std::endl;
            std::exit(1);
        }

        std::string infix;
        std::vector<interval_t> intervals;

        while (!in.eof())
        {
            in >> infix;
            if (in.eof())
            {
                break;
            }
            in >> intervals;
            trie_t subtrie = trie.getSubtrie(infix, true);
            std::swap(subtrie.root->data, intervals);
        }
    }

    int lookup(const std::string& str, size_t remaining_turns)
    {
        int answer = 0; // SKIP

        trie_t trie_entry = trie.getSubtrie(str);

        if (trie_entry.root != nullptr)
        {
            const interval_list_t& intervals = trie_entry.root->data;

            answer = getAnswer(intervals, remaining_turns);
        }

        return answer;
    }

private:
    static int getAnswer(const interval_list_t& intervals, size_t remaining_turns)
    {
        for (interval_list_const_iter_t it = intervals.cbegin(); it != intervals.cend(); ++it)
        {
            const interval_t& interval = *it;
       
            if (remaining_turns >= interval.start && remaining_turns <= interval.end)
            {
                switch (interval.turn) {
                    case APPEND:
                        return 1;
                    case PREPEND:
                        return 2;
                    default:
                        return 0;
                }
            }
        }

        return 0;
    }
};

int main(int argc, const char* argv[])
{
    if (argc <= 1 || argc >= 5)
    {
        std::cout << "Usage: " << argv[0] << " <username> [K [mode]]" << std::endl;
        std::cout << "       mode must be either ONESIDE or TWOSIDE, default is TWOSIDE" << std::endl;
        std::cout << "       K is the number of characters issued in each round" << std::endl;
        return 0;
    }

    int mode = 2; // TWOSIDE
    size_t turns = 200;
    size_t number_of_games = 10000;

    std::string username(argv[1]);

    if (argc >= 3)
    {
        turns = std::atoi(argv[2]);
    }

    if (argc == 4) 
    {
        std::string strmode(argv[3]);
        if (strmode == "ONESIDE") 
        {
            mode = 1;
        } 
        else if (strmode == "TWOSIDE") 
        {
            mode = 2;
        }
        else 
        {
            std::cerr << "invalid mode" << std::endl;
            return 1;
        }
    }

    LookupTable table;
    std::cout << "[loading precomputed turns...]" << std::flush;

    table.loadFromFile(std::string("precomputed_turns") + 
                       std::to_string(static_cast<unsigned long long>(mode)) +
                       ".dat");

    std::cout << "\r[loading precomputed turns... done!]" << std::endl;

    Game game(username);

    game.start_set(mode, number_of_games, turns);

    for (size_t j = 0; j < number_of_games; ++j)
    {
        std::cout << "\rPlaying game #" << (j + 1) << "...";

        std::string word = "";
        for (size_t i = 0; i < turns; ++i)
        {
            char ch = game.get_char();
            std::string tmp = word + ch;

            int answer = table.lookup(tmp, turns - i);

            if (answer == 1)
            {
                word = tmp;
            } 

            if (answer == 2)
            {
                word = ch + word;
            }

            game.put_ans(answer);
        }

        game.end_game();
    }

    std::cout << "\rGame is finished, see results in '" << username;
    switch (mode)
    {
        case 2:
            std::cout << "-0-lr.txt'" << std::endl;
            break;
        case 1:
            std::cout << "-0-r.txt'" << std::endl;
            break;
        default:
            assert(0);
    }
}
