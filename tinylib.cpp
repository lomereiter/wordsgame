#include <cstdio>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <string>
#include <ctime>
#include <set>
#include <random>
#include "tinylib.h"

using namespace std;

static int curset = 0;

string Game::get_game_type() {
  if (gametype == 1) return "r"; else return "lr";
}

void Game::start_log() {
  string filename = cliname + "-" + std::to_string(static_cast<unsigned long long>(curset)) + "-" + get_game_type() +".txt";
  blog.open(filename.c_str());
  blog << "Player is " << cliname << endl << endl;
  sumlength = 0; sumcount = 0;
}

void Game::write_move_map() {
  blog << '[';
  for (int i = 0; i < curpos; i++) blog << quests[i];
  blog << ']' << endl << "[";
  for (int i = 0; i < curpos; i++) blog << answs[i];
  blog << ']' << endl;  
}

void Game::insert_succ(string word, int num) {
  write_move_map();
  sumcount++;
  sumlength += word.length();
  lengths[num] = word.length();
  blog << '#' << num << ": Success! Word is " << word << ", length is " << word.length() << endl << endl;
}

void Game::insert_fail(string word, int num) {
  write_move_map();
  sumcount++;
  lengths[num] = 0;
  blog << '#' << num << ": Fail! Word is " << word << endl << endl;
}

void Game::finish_log() {
  if (sumcount == 0) blog << "No games were played"; else {
  double ev = (double)sumlength / sumcount;
  blog << "-------------------------" << endl;
  blog << "Expected value is " << fixed << ev << endl;

  double variance = 0;
  for (int i = 0; i < curgame; i++) variance += lengths[i] * lengths[i] - ev * ev;
  variance /= curgame;
  blog << "Variance is " << fixed << variance << endl;
  
  double conf_int_l = ev - CGAMMA * sqrt(variance / curgame);
  double conf_int_r = ev + CGAMMA * sqrt(variance / curgame);
  
  blog.precision(8);
  blog << "Confidence interval is [" << conf_int_l << ", " << conf_int_r << "]" << endl;
  }
  blog.close();
}


void Game::insert_word(string word) {
  words.insert(word);
}

int Game::have_word(string word) {
  return words.find(word) != words.end();
}

Game::Game(string name) : dist(0, 25) {
  //if (inited) return 1;
  ifstream dict;
  dict.open("dict2.txt", ifstream::in);
  //if (dict.fail()) return 0;
  while (!dict.eof()) {
    string word;
    dict >> word;
    if (word != "") insert_word(word);
  }
  dict.close();
  cliname = name;
  //inited = 1;
  named = 0; ingame = 0; setnum = 0;
  //return 1;
}

int Game::start_test_set(int type, int game_count, int length, int seed) {
//type = {1 - only right append is allowed, 2 - two-sided append}
//game_count = number of games
//length = number of symbols in game
//seed = set randseed
  if (ingame) return 0;
  
  gen.seed(static_cast<unsigned>(seed));
  if (type < 1 || type > 2) return 0;
  gametype = type;
  if (game_count < 1) return 0;
  gamecount = game_count;
  if (length < 1 || length > MAXGAME) return 0;
  gamelength = length;
  ingame = 1;
  curgame = 0; curpos = 0; curword = "";
  waitforans = 0;
  start_log();
  return 1;
}

int Game::start_set(int type, int game_count, int length) {
  //type = {1 - only right append is allowed, 2 - two-sided append}
  //game_count = number of games
  //length = number of symbols in game
    if (ingame) return 0;
  //  srand(0xDEAD);
    gen.seed(static_cast<unsigned>(time(0)));
    if (type < 1 || type > 2) return 0;
    gametype = type;
    if (game_count < 1 || length > MAXSET) return 0;
    gamecount = game_count;
    if (length < 1 || length > MAXGAME) return 0;
    gamelength = length;
    ingame = 1;
    curgame = 0; curpos = 0; curword = "";
    waitforans = 0;
    start_log();
    return 1;
  }


char Game::get_char() {
  if (!ingame || waitforans || curpos == gamelength) return '#';
  waitforans = 1;
  cursymb = dist(gen) + ALP_F;
  quests[curpos] = cursymb;
  answs[curpos] = '?';
  curpos++;
  return cursymb;
}

int Game::put_ans(int ans) {
  if (!ingame || !waitforans || ans < 0 || ans > gametype) return 0;
  if (ans == 0) answs[curpos - 1] = '-';
  if (ans == 1) {
    answs[curpos - 1] = '>';
    curword += cursymb;
  }
  if (ans == 2) {
    answs[curpos - 1] = '<';
    curword = cursymb + curword;
  }
  waitforans = 0;
  return 1;
}

int Game::end_game() {
  if (!ingame) return 0;
  if (have_word(curword))
    insert_succ(curword, curgame); else insert_fail(curword, curgame);
  curgame++; if (curgame == gamecount) {
    finish_log();
    ingame = 0;
    curset++;
    return 2;
  }
  curpos = 0; curword = ""; waitforans = 0;
  return 1;
}
