#ifndef LENGTH_EXPECTATION_ARRAY_HPP
#define LENGTH_EXPECTATION_ARRAY_HPP

#include "common.hpp"
#include <vector>

struct LengthExpectationArray
{
    static size_t turns_to_precompute;

    LengthExpectationArray();
    float& operator[](size_t);
    const float& operator[](size_t) const;
    size_t size() const;

private:
    std::vector<float> container;
};

#endif
