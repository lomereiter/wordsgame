#include "lengthexpectationarray.hpp"

size_t LengthExpectationArray::turns_to_precompute = 200;

LengthExpectationArray::LengthExpectationArray()
{
    container.resize(turns_to_precompute + 1);
}

float& LengthExpectationArray::operator[](size_t index)
{
    return container[index];
}

const float& LengthExpectationArray::operator[](size_t index) const
{
    return container[index];
}

size_t LengthExpectationArray::size() const
{
    return container.size();
}
